function [ THETAk1, PK1 ] = RLS( yk, phik, thetak, pk, lmb )
%RLS Solve a recursive least squares (RLS) problem.
%   [thetakl,pkl] = RLS( yk, phik, thetak, pk, lmb ),
%
%       yk = phik * thetak
%       yk'= thetak * phik => yk = phik' * thetak'
%   For the parameters:
%
%       yk          The output variable
%       phik        The input variable
%       thetak      The coefficient vector
%       pk          The P(k) matrix
%       lmb         The forgetting parameter
%
% Guido Monchen, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 


    LF = pk * phik' / (lmb * eye(size(yk,1)) + phik * pk * phik');
    THETAk1 = thetak + LF * (yk - phik * thetak);
    PK1 = (1/lmb) * (pk - LF * phik * pk);

end

