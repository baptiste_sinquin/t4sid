function [M_HAT,NORM_RESIDUAL] = ho_quarks(I,J,s,r,d,Uid,Yid,opt)
% Alternating Least Squares for estimating the QUARKS
% Example of possible model structures: Finite Impulse Responses, AutoRegressive
% 
% INPUT:
% I: row vector of dimension equal to the tensor order, contains the dimensions of the input grid
% J: row vector of dimension equal to the tensor order, contains the dimensions of the output grid
% s: temporal order
% r: Kronecker rank
% d: tensor order
% Uid: cell Ntx1, contains the input data
% Yid: cell Ntx1, contains the output data
% opt: 
% opt.pos: opt.pos: scalar. if set to 1, the factor matrices are strictly positive elementwise
%	   else, random matrices
% OUTPUT: 
% M_hat: cell sxrxd, containing the estimated factor matrices
% norm_residual: vector of size itermax (maximum number of iterations in the ALS), containing the residuals in Frobenius norm 
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

if exist('opt','var') && isfield(opt,'pos'), pos = opt.pos; else pos = 0; end
if exist('opt','var') && isfield(opt,'lambda'), lambda = opt.lambda; else lambda = 0; end
	
Nid = length(Yid);
M_HAT = cell(s,r,d);
% initial guesses
for i = 1:s
    for j = 1:r
        for x = 1:d        
            M_HAT{i,j,x} = abs(randn(J(x),I(x)));
        end
    end
end
% unfolded outputs
[Y_unfolded,nbar] = unfold_data(d,J,s+1,Nid,Yid);
% start iterations
itermax = 30;
NORM_RESIDUAL = zeros(itermax,1);
normalization = 0;
iter = 1;
old = Inf;
grad = Inf;
while iter < itermax && grad > 1e-6
    for x = 1:d
        modes = [1:x-1 x+1:d];
        mat = zeros(I(x)*s*r,Nid*nbar(x));
        for k = s+1:Nid
            for i = 1:s
                for j = 1:r
                    temp = tmprod(permute(Uid{k-i},d:-1:1),{M_HAT{i,j,modes}},modes);
                    mat((i-1)*I(x)*r+(j-1)*I(x)+1:(i-1)*I(x)*r+j*I(x),(k-s-1)*nbar(x)+1:(k-s)*nbar(x)) = ...
                                reshape(permute(temp,[x [d:-1:x+1] [x-1:-1:1]]),I(x),nbar(x));
                    %mat((i-1)*I(x)*r+(j-1)*I(x)+1:(i-1)*I(x)*r+j*I(x),(k-s-1)*nbar(x)+1:(k-s)*nbar(x)) = ...
                    %    tens2mat(Uid{k-i},d-x+1)*kron({M_hat{i,j,modes}})';
                end
            end
        end
        if pos == 0
            temp = Y_unfolded{x}*mat'/((mat*mat'+lambda*eye(I(x)*s*r)));
        elseif pos == 1
            % start ADMM 
            imat = inv(mat*mat'+lambda*eye(I(x)*s*r));
            uk = zeros(I(x)*s*r,J(x));
            zk = zeros(I(x)*s*r,J(x));
            norm_rp = Inf; iterADMM = 1;
            while iterADMM < 100 && norm_rp > 1e-3
                xk = imat*(mat*Y_unfolded{x}'+zk-uk);
                zk = max(0,xk+uk);
                rp = xk-zk;
                uk = uk+rp;
                norm_rp = norm(rp,'fro');
                %
                iterADMM = iterADMM+1;
            end
            temp = xk';
        end
        %
        for i = 1:s
            for j = 1:r
                M_HAT{i,j,x} = temp(:,(i-1)*I(x)*r+(j-1)*I(x)+1:(i-1)*I(x)*r+j*I(x));
                if d == 2 && x == 1 && normalization == 1
                     M_HAT{i,j,x} =  normc(M_HAT{i,j,x}); 
                end
            end
        end
    end    
    new = norm(Y_unfolded{x}-temp*mat,'fro');
    grad = abs(new-old);
    old = new;
    NORM_RESIDUAL(iter,:) = new/norm(Y_unfolded{x},'fro');
    %
    iter = iter+1;
end


end
