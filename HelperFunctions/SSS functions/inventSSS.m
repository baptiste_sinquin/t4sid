function [SSS]=inventSSS(N,nrmax,ncmax,nwmax)
% SSS constructors:
% P,R,Q, D, U,W,V
%
% Reference: J. Rice, "Efficient Algorithms for Distributed Control: A Structured Matrix Approach",
% PhD thesis, TU Delft, 2010.
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

n_r = zeros(N+1,1);% #output
n_c = zeros(N+1,1);% #input
n_w = zeros(N+1,1);% order
for i = 1:N+1
   n_r(i) = 1;%floor(rand*nrmax)+1;
   n_c(i) = 1;%floor(rand*ncmax)+1;
   n_w(i) = 2;%floor(rand*nwmax)+1;
end

scale = 2;
scaleup = 0.5;

P = cell(N,1); R = cell(N,1); Q = cell(N,1); 
D = cell(N,1);
U = cell(N,1); W = cell(N,1); V = cell(N,1);
for i = 2:N
   D{i,1}=randn(n_r(i),n_c(i))/scale;
   
   V{i,1}=randn(n_w(i-1),n_c(i))/scale;
   U{i,1}=randn(n_r(i),n_w(i))/scale;
   Q{i,1}=randn(n_w(i+1),n_c(i))/scale;
   P{i,1}=randn(n_r(i),n_w(i))/scale;
    
   W{i,1}=randn(n_w(i-1),n_w(i));
   R{i,1}=randn(n_w(i+1),n_w(i));
   
   W{i,1}=W{i,1}/srad(W{i,1})*rand;
   while srad(W{i,1}) < 0.6 && srad(W{i,1}) > 0.99
   	W{i,1}=W{i,1}/srad(W{i,1})*rand;
   end
   R{i,1}=R{i,1}/srad(R{i,1})*rand;
   while srad(R{i,1}) < 0.6 && srad(R{i,1}) > 0.99
   	R{i,1}=R{i,1}/srad(R{i,1})*rand;
   end
end
i = 1;
D{1,1}=randn(n_r(1),n_c(1))/scale;
V{i,1}=randn(1,n_c(i))/scale;
U{i,1}=randn(n_r(i),n_w(i))/scale;
Q{i,1}=randn(n_w(i+1),n_c(i))/scale;
P{i,1}=randn(n_r(i),n_w(i))/scale;
W{i,1}=randn(1,n_w(i));
R{i,1}=randn(n_w(i+1),n_w(i));

%now make SSS object:
SSS.P=P;
SSS.R=R;
SSS.Q=Q;
SSS.D=D;
SSS.U=U;
SSS.W=W;
SSS.V=V;
SSS.N=N;
SSS.n_r=n_r;
SSS.n_c=n_c;
SSS.n_w=n_w;

