function [SSSmatrix]= construct_new_SSS(SSS)
% Reference: J. Rice, "Efficient Algorithms for Distributed Control: A Structured Matrix Approach",
% PhD thesis, TU Delft, 2010.
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

D=SSS.D;

N=SSS.N;
n_r=SSS.n_r;
n_c=SSS.n_c;
n_w=SSS.n_w;

[SSSLT]= construct_new_LT_lowmemory_SSS(SSS);
[SSSUT]= construct_new_LT_lowmemory_SSS(SSStranspose(SSS))';
SSSmatrix=SSSLT+SSSUT;

% fill in the diagonal first 
for i = 1:N
	% locate in the matrix
	r = sum(n_r(1:i-1))+1;
	c = sum(n_c(1:i-1))+1;
	%
	SSSmatrix(r:r+n_r(i)-1,c:c+n_c(i)-1) = D{i};
end


