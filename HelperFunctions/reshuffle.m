function [ Y ] = reshuffle( X, m, n )
%RESHUFFLE The reshuffling operation for the block matrix X
% [Y] = reshuffle( X, m, n ) returns the reshuffled matrix Y for the
% parameters:
%
% INPUT:
% X: The block-matrix to be reshuffled
% m: The number of row blocks
% n:  The number of column blocks
%
% Guido Monchen, April 2017
% Copyright (c) 2018, Delft Center of Systems and Control 

m2 = size(X,1) / m;
n2 = size(X,2) / n;

Y = zeros(m * n, m2 * n2);

for j = 1:n
	for i = 1:m
	    Y((j-1) * n + i, :) = vec(X((i-1)*m2+1:i*m2,(j-1)*n2+1:j*n2));
	end
end

end

