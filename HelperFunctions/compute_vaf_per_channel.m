function VAF = compute_vaf_per_channel(Yref,Yhat,J)
% 
% INPUT:
% Yref: cell Ntx1 (Nt is the number of temporal samples in the dataset), reference output
% Yhat: cell Ntx1, estimated output
% J: row vector of dimension equal to the tensor order, contains the dimensions of the output grid
% OUTPUT:
% VAF: vector of size prod(J)x1, contains the VAF for each output channel between the original and estimated data
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

Nt = length(Yref);
vecYref = zeros(prod(J),Nt);
vecYhat = zeros(prod(J),Nt);
for k = 1:Nt
    vecYref(:,k) = vec(Yref{k});
    vecYhat(:,k) = vec(Yhat{k}); 
end
%figure,plot(vecYref(:)),hold on,plot(vecYhat(:)),legend('original','estimated'),drawnow
%
VAF = vaf(vecYref(:),vecYhat(:));

end
