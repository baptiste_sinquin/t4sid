function [B,C,sumBC] = genKronPairs(N,m,n,opts)
% genKronPairs - generates a given number of Kronecker products with given
% properties for testing purposes.

% INPUTS:
%   N   -   number of Kronecker pairs
%   m,n -   Kronecker matrix size m by n
%   opts-   generation options:
%               opts.decay = ###
%               opts.symmetric = true/false               

% OUTPUTS:
%   B,C -   generated Kronecker pairs given as cell arrays
% 
% Peter Varnai, August 2017
% Copyright (c) 2018, Delft Center of Systems and Control 

% default parameters
if (~isfield(opts,'decay'));
    opts.decay = 0;
end
if (~isfield(opts,'symmetric'));
    opts.symmetric = false;
end
if (~isfield(opts,'type'));
    opts.type = '';
end


B = cell(N,1);
C = cell(N,1);
sumBC = 0;

for i = 1:N
    if (strcmp(opts.type,'toeplitz') || strcmp(opts.type,'toeplitz-descent'))
        Bc = randn(m,1); Br = randn(n,1);
        Cc = randn(m,1); Cr = randn(n,1);
        if (strcmp(opts.type,'toeplitz-descent')) 
           Bc = sort(abs(Bc),'descend').*sign(Bc); Br = sort(abs(Br),'descend').*sign(Br);
           Cc = sort(abs(Cc),'descend').*sign(Cc); Cr = sort(abs(Cr),'descend').*sign(Cr);
           Bc(1) = max(abs(Bc(1)),abs(Br(1)))*sign(Bc(1));
           Cc(1) = max(abs(Cc(1)),abs(Cr(1)))*sign(Cc(1));
        end
        Br(1) = Bc(1); Cr(1) = Cc(1);        
        B{i} = toeplitz(Bc,Br);
        C{i} = toeplitz(Cc,Cr);
    elseif strcmp(opts.type,'SSS')
        temp = inventSSS(n,2,1,1);
        B{i} = construct_new_SSS(temp)*1e2;
        %figure,imagesc(log10(abs(B{i})))
        temp = inventSSS(n,2,1,1);
        C{i} = construct_new_SSS(temp)*1e2;
    else
        B{i} = randn(m,n);
        C{i} = randn(m,n);
    end

    if (~isempty(opts.decay))
       B{i} = exp(-i*opts.decay/2)*B{i};
       C{i} = exp(-i*opts.decay/2)*C{i};
    end
    if (~isempty(opts.symmetric))
      if (opts.symmetric == true)
         B{i} = (B{i}+B{i}')/2; 
         C{i} = (C{i}+C{i}')/2;
      end
    end

    % Sum Kronceker terms if requested by output
    if (nargout == 3)
        sumBC = sumBC + kron(B{i},C{i}); 
    end
end


end

