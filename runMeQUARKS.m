% QUARKS: Identification of multi-dimensionals autoregressive models with
% sums-of-Kronecker parametrization
% 
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

clc; clear;
close all;

% generate the model 
I = [2 3 2];            % dimension of input grid
J = [4 5 2];            % dimension of output grid
d = length(I);          % tensor order
r = 2;                  % Kronecker rank
s = 5;                  % length of FIR filter
opt.pos = 0;
opt.sss = 0;
M = get_fir_model(d,r,s,I,J,opt); 

% generate input-output data
Nt = 1e3;               % number of temporal samples
Nid = floor(2/3*Nt);	% number of points in the identiifcation batch
Nval = Nt-Nid;          % number of points in the validation batch
snr = 20;               % signal to noise ratio
[Uid,Yid] = get_fir_data(I,J,Nid,snr,M);
[Uval,Yval] = get_fir_data(I,J,Nval,snr,M);

% estimate a quarks model
opt.lambda = 0; 		% regularization parameter
tic
[M_hat,norm_residual] = ho_quarks(fliplr(I),fliplr(J),s,r,d,Uid,Yid,opt);   
time = toc;
figure,semilogy(norm_residual),title('Residual least squares for ALS'),grid on,drawnow

% check on validation data
Yhat_val = get_fir_output(J,M_hat,Uval);
VAF = compute_vaf_per_channel(Yval,Yhat_val,J);
fprintf('QUARKS. VAF: %6.2f Time(s): %6.2f \n',VAF,time);



