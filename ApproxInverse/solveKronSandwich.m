function x = solveKronSandwich(A, B, C, indices, reg)
% solveKronSandwich
% Solves min(x) ||A - sum_i^M B_i (I kron diag(x1,x2,...,xN)) C_i||
% for a suitably sized I
% 
% Peter Varnai, August 2017 
% Copyright (c) 2017, Delft Center of Systems and Control 

n = indices{end}(end);  % total number of unknowns
M = size(B,2)/n;        % Number of summands B_i, C_i = M
N = size(C,1)/M;        % Number of unknown vectors = N


% Apply economic QR decomposition from left and right
[QB,R] = qr(B,0);
[QC,L] = qr(C',0); L = L';      % QC not transposed twice
Atilde = QB'*A*QC;

% Store number of rows in R and initialize H matrix
r = size(R,1);
H = zeros(r, M*N*n);

% Cycle through each block-row of R, by summand number and unknown number
for iM = 1:M
    for iN = 1:N
        % Make sure to stay within rows of R
        colIndices = (iM-1)*n + indices{iN};
        if (colIndices(1) > r)
            break;
        elseif (colIndices(end) > r)
            colIndices = colIndices(1):r;
        end        
        % Cycle through each column of L
        for j = 1:M*N           
            % Cycle through all unknowns by M and N
            for kM = 1:M
               for kN = 1:N
                  % Assemble coefficient for unknown N belonging to this
                  % column of L
                  H(colIndices,(j-1)*n+indices{kN}) = H(colIndices,(j-1)*n+indices{kN}) + R(colIndices,(kM-1)*n + indices{kN}) * L((kM-1)*N+kN,j);                   
               end
            end           
        end
    end
end

% rearrange H for least squares solution of unknowns
Htilde = zeros(M*N*r,n);
for i = 1:M*N
    Htilde((i-1)*r+1:i*r,:) = H(:, (i-1)*n+1:i*n);
end

if (nargin < 5)
    x = Htilde\vec(Atilde);
else
    Htilde = [Htilde; eye(n)];
    x = Htilde\[vec(Atilde); reg];
end



end

