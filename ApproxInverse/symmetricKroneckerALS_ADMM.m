function X = symmetricKroneckerALS_ADMM(H,fk,X,opts,rho)
%symmetricKroneckerALS_ADMM Inner ADMM iterations of symmetricKroneckerALS
% to enforce symmetry of the solution.
% 
% Peter Varnai, August 2017 
% Copyright (c) 2017, Delft Center of Systems and Control 

% Problem size
n = size(X{1},1);
N = length(X);

% options for solving triangular equations fast
linopts.UT = true;

% ADMM parameters & initialization
iterADMM = 0;
admmCondition = true;
Z = X;
U = cell(N,1);
[U{:}] = deal(zeros(n,n));
reg = zeros(N*n,1);
alpha = 1;
res = 1e8;
% ADMM loop
UU = U;
ZZ = Z;
while (admmCondition)
    iterADMM = iterADMM + 1;
    
    OU = U;
    OZ = Z;
    
    % Solve for normal equation
    [QH,RH] = qr([H; rho*eye(N*n)]);
    for i = 1:n        
        for j = 1:N
           reg((j-1)*n+1:j*n) = ZZ{j}(:,i) - UU{j}(:,i); 
        end
        fki = QH'*[fk{i}; rho*reg];
        x = linsolve(RH,fki, linopts);
        for j = 1:N
            X{j}(:,i) = x((j-1)*n+1:j*n);            
        end
    end  
    % Solve transposed equation
    for i = 1:n            
        for j = 1:N
           reg((j-1)*n+1:j*n) = X{j}(i,:)' + UU{j}(i,:)'; % transposed!
        end
        fki = QH'*[fk{i}; rho*reg];
        z = linsolve(RH,fki, linopts);
        for j = 1:N
            Z{j}(i,:) = z((j-1)*n+1:j*n)';            
        end
    end  
    % Update dual variable & acceleration
    oalpha = 1;%;alpha;
    alpha = (1 + sqrt(4*alpha^2+1))/2;
    for j = 1:N
        U{j} = UU{j} + X{j} - Z{j};
        ZZ{j} = Z{j} + (oalpha-1)/alpha*(Z{j} - OZ{j});
        UU{j} = U{j} + (oalpha-1)/alpha*(U{j} - OU{j});
    end

    oldres = res;
    res = norm(fki - RH*z);
    fprintf('Objective value: %f\n',res);
    admmCondition = (iterADMM < opts.maxADMMIter) && (abs(oldres-res)>1e-10);
end

end

