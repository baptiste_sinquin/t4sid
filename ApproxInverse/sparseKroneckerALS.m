function [X,Y, iterNum, relchange, res] = sparseKroneckerInverse(F, B, C, X0, Y0, PX, PY, N, opts)
% sparseKroneckerSumInverse
% Solves min(X,Y) ||A - (sum_i^M (B_i kron C_i)) * (sum_j^N(X_j kron Y_j))||_F
% for a given sparsity patterns of each X and Y
% Assume X, Y are each square matrices

% INPUTS:
% F         - n^2 by n^2 matrix of the optimization problem or P by 2 cell array of Kronecker pairs.
% B,C       - n by n matrices of the problem, provided as cell arrays
% X0,Y0     - optional initial n by n matrices for the unknowns
% opts      - structure of algorithm options:
%   maxIter   - maximum ALS iterations
%   relSol    - termination tolerance for relative change in variables
%   relObj    - termination tolerance for relative change in objective
%   useFast   - flag whether or not to employ Nesterov-type acceleration
%   normalize - whether or not to normalize Kronecker pairs in iterations
%   showInfo  - flag to display convergence information

%OUTPUTS:
% X,Y       - cell array of n by n matrices describing the solution
% iterNum   - actual number of ALS iterations required for given tolerance
% relchange - relative change of solution during ALS iterations 
% res       - achieved residual error

% Note that the full Kronecker matrices are only formed and calculated with
% if either opts.relObj is present or 'res' is specified as an output.
% 
% Peter Varnai, August 2017 
% Copyright (c) 2017, Delft Center of Systems and Control 

%% Parse input
% Convert input to cells for easier handling
if (~isempty(X0) && ~iscell(X0))   
   X0 = num2cell(X0,[1 2]);
end
if (~isempty(Y0) && ~iscell(Y0))
   Y0 = num2cell(Y0,[1 2]);
end
if (~iscell(PX))
   PX = num2cell(PX,[1,2]);
end
if (~iscell(PY))
   PY = num2cell(PY,[1,2]);
end
if (~iscell(B))   
   B = num2cell(B,[1 2]);
   C = num2cell(C,[1 2]);
end
M = length(B);      % number of sum terms B_i, C_i
n = size(B{1},1);

% Check if input is composed of size n matrices
if (iscell(F))
   P = size(F,1); 
   for i=1:P
       if (size(F{i,1},1) ~= n || size(F{i,1},2) ~= n || size(F{i,2},1) ~= n || size(F{i,2},2) ~= n)
           fprintf('Error: matrix F should be of consistent dimension\n');
       end
    end
else
    if (size(F,1) ~= n^2 || size(F,2) ~= n^2)
        fprintf('Error: matrix F should be of consistent dimension\n');
    end
end
for i=1:M
    if (size(B{i},1) ~= n || size(B{i},2) ~= n ||...
            size(C{i},1) ~= n || size(C{i},2) ~= n)
        fprintf('Error: Matrices Bi, Ci should have the same size\n');
        break;        
    end
end

% Default maximum iterations & relative tolerance
if (~isfield(opts,'relSol'))
    opts.relTol = 1e-6;
end
if (~isfield(opts,'maxIter'))
    opts.maxIter = 50;
end
if (~isfield(opts,'maxADMMIter'))
    opts.maxADMMIter = 200;
end
if (~isfield(opts,'useFast'))
    opts.useFast = false;
end
if (~isfield(opts,'relObj'))
    opts.relObj = [];
end
if (~isfield(opts,'normalize'))
    opts.normalize = true;
end
if (~isfield(opts,'showInfo'))
    opts.showInfo = false;
end
if (~isfield(opts,'fastRestart'))
    opts.fastRestart = false;
end
if (~isfield(opts,'symmetric'))
	opts.symmetric = false;
end

% Check if there will be a need to evaluate objective value
if (nargout == 5 || ~isempty(opts.relObj))
    normFlag = true;
else
    normFlag = false;
end

%% Initialize ALS
% Initialize solution (X and Y are cell arrays for each N unknown pair!)
X = cell(N,1);
Y = cell(N,1);
for i = 1:N
    if (isempty(X0))
        X{i} = randn(n);
    else
        X{i} = X0{i};
    end
    if (isempty(Y0))
        Y{i} = randn(n);
    else
        Y{i} = Y0{i};
    end
    X{i} = X{i}.*PX{i};
    Y{i} = Y{i}.*PY{i};
    
    % Normalize initial matrices if necessary
    if (opts.normalize)
        ratio = sqrt(norm(X{i},'fro')/norm(Y{i},'fro'));
        X{i} = X{i}/ratio;
        Y{i} = Y{i}*ratio;
    end
end

% options for solving triangular equations fast
linopts.UT = true;

% compute Kronecker rearrangement of matrix F
if (iscell(F))
    f = cell(size(F));
    for i = 1:P
       f{i,1} = vec(F{i,1});
       f{i,2} = vec(F{i,2});
    end
else
    RF = rearrangeKronecker(F,n,n,n,n);
end

% for each column of each X and Y, store the row indices where we have an entry
xColEntryIndices = cell(N, n);
yColEntryIndices = cell(N, n);
% Initialize solutions for entries in each column of X and Y
sparseX = cell(N,n);
sparseY = cell(N,n);
% Computation of the above
for i = 1:N
    for j = 1:n
        xColEntryIndices{i,j} = find(PX{i}(:,j));
        sparseX{i,j} = X{i}(xColEntryIndices{i,j},j);        
    end
    for j = 1:n
        yColEntryIndices{i,j} = find(PY{i}(:,j));
        sparseY{i,j} = Y{i}(yColEntryIndices{i,j},j);  
    end
end

% store indices of each column xi, yi, if they were merged in a vector
xIndices = cell(N,n);
yIndices = cell(N,n);
for j = 1:n
    xIndices{1,j} = 1:length(xColEntryIndices{1,j});
    for i = 2:N    
        prev = xIndices{i-1,j}(end);
        xIndices{i,j} = prev+1:prev+length(xColEntryIndices{i,j});
    end
end
for j = 1:n
    yIndices{1,j} = 1:length(yColEntryIndices{1,j});
    for i = 2:N    
        prev = yIndices{i-1,j}(end);
        yIndices{i,j} = prev+1:prev+length(yColEntryIndices{i,j});
    end
end
 
%% ALS iterations
iter = 0;
loopCondition = true;
res = zeros(opts.maxIter,1);
relchange = zeros(opts.maxIter,1);
fk = cell(n,1);
nu = 0.999;
alpha = 1;
newNorm = 1e8;
Ytilde = Y;             % accomodates acceleration
while(loopCondition)
    iter = iter + 1;
    if (opts.showInfo)
       fprintf('Iteration %d:\n',iter); 
    end
    
    % Save previous iteration's solution to evaluate change later
    OX = X;
    OY = Y;
    
    % Solve for each column of every X_i at the same time
    % Assemble 'C' matrix which is common for all X_i's + factorize
    yC = zeros(M*N, n^2);
    for k = 1:n
        for i = 1:M
            for j = 1:N
                yC((i-1)*N+j,(k-1)*n+1:k*n) = sparseY{j,k}'*C{i}(:,yColEntryIndices{j,k})';
            end
        end
    end
    [QyC,LyC] = qr(yC',0); LyC = LyC';      % QC not transposed, as it's used as it is computed here    
    H = cell(1,n);
	for i = 1:n
        % Assemble BMAT
        sparseBlen = xIndices{N,i}(end);
        BMAT = zeros(n, sparseBlen*M);
        for j = 1:M
            for k = 1:N
                BMAT(:,(j-1)*sparseBlen+xIndices{k,i}) = B{j}(:,xColEntryIndices{k,i});
            end
        end
        [QBMAT,RBMAT] = qr(BMAT,0);
        H{i} = computeH_sparse(M,N,xIndices(:,i),RBMAT,LyC);
        
		if (iscell(F))
			fk{i} = 0;
			for j=1:P
				fk{i} = fk{i} + vec((QBMAT'*f{j,1}((i-1)*n+1:i*n))*(f{j,2}'*QyC));
            end
        else
            fk{i} = vec(QBMAT'*(RF((i-1)*n+1:i*n,:)*QyC));
        end
	end
	if (opts.symmetric)
		X = symmetricKroneckerALS_ADMM(H,fk,X,opts,n);
    else		
		for i = 1:n
            [QH,RH] = qr(H{i},0);
			fk{i} = QH'*fk{i};
			x = linsolve(RH,fk{i},linopts);
			for k = 1:N
                sparseX{k,i} = x(xIndices{k,i}); 
				X{k}(xColEntryIndices{k,i},i) = sparseX{k,i};            
			end
		end
	end
    
    % Solve for each column of every Y_i at the same time
    % Assembe 'B' matrix which is common for all Y_i's
    xB = zeros(M*N, n^2);
    for k = 1:n
        for i = 1:M
            for j = 1:N
                xB((i-1)*N+j,(k-1)*n+1:k*n) = sparseX{j,k}'*B{i}(:,xColEntryIndices{j,k})';
            end
        end
    end     
    [QxB,LxB] = qr(xB',0); LxB = LxB';      % QC not transposed, as it's used as it is computed here
    H = cell(1,n);    
	for i = 1:n
        % Assemble CMAT
        sparseClen = yIndices{N,i}(end);
        CMAT = zeros(n, sparseClen*M);
        for j = 1:M
            for k = 1:N
                CMAT(:,(j-1)*sparseClen+yIndices{k,i}) = C{j}(:,yColEntryIndices{k,i});
            end
        end
        [QCMAT,RCMAT] = qr(CMAT,0);
        H{i} = computeH_sparse(M,N,yIndices(:,i),RCMAT, LxB);
        
		if (iscell(F))
			fk{i} = 0;
			for j=1:P
				fk{i} = fk{i} + vec((QCMAT'*f{j,2}((i-1)*n+1:i*n))*(f{j,1}'*QxB));
            end
        else
            fk{i} = vec(QCMAT'*(RF(:,(i-1)*n+1:i*n)'*QxB));
        end
    end
	if (opts.symmetric)
		Y = symmetricKroneckerALS_ADMM(H,fk,X,opts,n/1000);	
	else
        for i = 1:n
            [QH,RH] = qr(H{i},0);
			fk{i} = QH'*fk{i};
			y = linsolve(RH,fk{i},linopts);
			for k = 1:N
                sparseY{k,i} = y(yIndices{k,i}); 
				Y{k}(yColEntryIndices{k,i},i) = sparseY{k,i};            
			end
        end		
    end    
    
    % Calculate solution as a sum of X_i kron Y_i
    if (normFlag)
        % display objective value  
        oldNorm = newNorm;
        newNorm = evalObj(F,B,C,X,Y);
        relObj = abs(newNorm - oldNorm)/newNorm;
        res(iter) = newNorm;
        if (opts.showInfo)
            fprintf('\tobjective value: %f\n',newNorm);
        end
        if iter > 1
            if res(iter) > res(iter-1)*nu;
               alpha = 1;
               iter = iter - 1;
            end
        end
    end
    
    if (opts.useFast)
        oalpha = alpha;
        alpha = (1 + sqrt(4*alpha^2+1))/2;
        if (opts.fastRestart > 0)
            if (mod(iter,opts.fastRestart) == 0)
                oalpha = 1;
                alpha = 1;
            end
        end
        for i = 1:N
            Ytilde{i} = Y{i} + (oalpha-1)*(Y{i}-OY{i})/alpha;            
        end
    else
        for i = 1:N
            Ytilde{i} = Y{i};
        end
    end
    
    % Normalize each Kronecker pair
    if (opts.normalize)
        for i = 1:N
           ratio = sqrt(norm(X{i},'fro')/norm(Y{i},'fro'));
           X{i} = X{i}/ratio;
           Y{i} = Y{i}*ratio;
           Ytilde{i} = Ytilde{i}*ratio;
        end
    end
  
    
    
    % Evaluate relative change in solution
    relSol = 0;
    for i = 1:N
        relSol = max(relSol,  norm(X{i} - OX{i},'fro')/ norm(X{i},'fro'));
        relSol = max(relSol,  norm(Y{i} - OY{i},'fro')/ norm(Y{i},'fro'));
    end   
    relchange(iter) = relSol;
    if (opts.showInfo)
        fprintf('\trelative change: %f\n',relSol);
    end

    
    loopCondition = (relSol > opts.relSol) && (iter < opts.maxIter);
    if (~isempty(opts.relObj))
        loopCondition = loopCondition && (relObj > opts.relObj);
    end
end

iterNum = iter;
relchange = relchange(1:iterNum);
if (nargout == 5)
    res = res(1:iterNum);
end


end

