function [X,Y, iterNum, relchange, res] = denseKroneckerALS_gradient(F, B, C, X0, Y0, N, opts)
% denseKroneckerALS_gradient
% Solves min(X,Y) ||F - (sum_i^M (B_i kron C_i)) * (sum_j^N(X_j kron Y_j))||_F
% Assume X, Y are each full dense square matrices
% Solution is based on setting the gradient to zero in order to satisfy
% the stationarity condition as described in the paper
% "LOW-RANK APPROXIMATE INVERSE FOR PRECONDITIONING
% TENSOR-STRUCTURED LINEAR SYSTEMS"

% INPUTS:
% F         - P by 2 cell array of Kronecker product pairs
% B,C       - n by n matrices of the problem, provided as cell arrays
% X0,Y0     - optional initial n by n matrices for the unknowns
% opts      - structure of algorithm options:
%   maxIter   - maximum ALS iterations
%   relSol    - termination tolerance for relative change in variables
%   relObj    - termination tolerance for relative change in objective
%   useFast   - flag whether or not to employ Nesterov-type acceleration
%   normalize - whether or not to normalize Kronecker pairs in iterations
%   showInfo  - flag to display convergence information
%   symmetric - return symmetric matrices as solution, works for N=1

%OUTPUTS:
% X,Y       - cell array of n by n matrices describing the solution
% iterNum   - actual number of ALS iterations required for given tolerance
% res       - achieved residual error

% Note that the full Kronecker matrices are only formed and calculated with
% if either opts.relObj is present or 'res' is specified as an output.
% 
% Peter Varnai, August 2017 
% Copyright (c) 2017, Delft Center of Systems and Control 

%% Parse input
% Convert input to cells for easier handling
if (~iscell(X0))   
   X0 = num2cell(X0,[1 2]);
end
if (~iscell(Y0))
   Y0 = num2cell(Y0,[1 2]);
end
if (~iscell(B))   
   B = num2cell(B,[1 2]);
   C = num2cell(C,[1 2]);
end
M = length(B);      % number of sum terms B_i, C_i
P = size(F,1);      % number of sum terms in objective

% Check if input is composed of size n matrices
n = size(B{1},1);
for i=1:P
    if (size(F{i,1},1) ~= n || size(F{i,1},2) ~= n || size(F{i,2},1) ~= n || size(F{i,2},2) ~= n)
        fprintf('Error: matrix F should be of consistent dimension\n');
    end
end
for i=1:M
    if (size(B{i},1) ~= n || size(B{i},2) ~= n ||...
            size(C{i},1) ~= n || size(C{i},2) ~= n)
        fprintf('Error: Matrices Bi, Ci should have the same size\n');
        break;        
    end
end

% Default maximum iterations & relative tolerance
if (~isfield(opts,'relSol'))
    opts.relTol = 1e-6;
end
if (~isfield(opts,'maxIter'))
    opts.maxIter = 50;
end
if (~isfield(opts,'useFast'))
    opts.useFast = false;
end
if (~isfield(opts,'relObj'))
    opts.relObj = [];
end
if (~isfield(opts,'normalize'))
    opts.normalize = true;
end
if (~isfield(opts,'showInfo'))
    opts.showInfo = false;
end
if (~isfield(opts,'symmetric'))
    opts.symmetric = false;
end
if (~isfield(opts,'fastRestart'))
    opts.fastRestart = 0;
end

% Check if there will be a need to evaluate objective value
if (nargout >= 5 || ~isempty(opts.relObj))
    normFlag = true;
else
    normFlag = false;
end

%% Initialize ALS
% Initialize solution (X and Y are cell arrays for each N unknown pair!)
X = cell(N,1);
Y = cell(N,1);
for i = 1:N
    if (isempty(X0{1}))
        X{i} = randn(n);
    else
        X{i} = X0{i};
    end
    if (isempty(Y0{1}))
        Y{i} = randn(n);
    else
        Y{i} = Y0{i};
    end
    
    % Normalize initial matrices if necessary
    if (opts.normalize)
        ratio = sqrt(norm(X{i},'fro')/norm(Y{i},'fro'));
        X{i} = X{i}/ratio;
        Y{i} = Y{i}*ratio;
    end
end

% precalculate some matrices and their needed factorizations
ATAX = cell(M^2,1);
ATAY = cell(M^2,1);
for i = 1:M
    for j = 1:M
       ATAX{(j-1)*M+i} = B{i}'*B{j};
       ATAY{(j-1)*M+i} = C{i}'*C{j};
    end    
end

ATFX = cell(M*P,1);
ATFY = cell(M*P,1);
for i = 1:M
    for j = 1:P
       ATFX{(j-1)*M+i} = B{i}' * F{j,1};
       ATFY{(j-1)*M+i} = C{i}' * F{j,2};
    end    
end

  
%% ALS iterations
iter = 0;
loopCondition = true;
res = zeros(opts.maxIter,1);
relchange = zeros(opts.maxIter,1);
alpha = 1;
newNorm = 1e8;
Ytilde = Y;             % accommodates acceleration
while(loopCondition)
    iter = iter + 1;
    if (opts.showInfo)
       fprintf('Iteration %d:\n',iter); 
    end
    
    % Save previous iteration's solution to evaluate change later
    OX = X;
    OY = Y;
    
    % Solve for X's (based on Ytilde - for possibly accelerated version)
    % Assemble H matrix
    H = zeros(N*n,n);
    for t = 1:N
       ind = (t-1)*n+1:t*n;
       for j = 1:length(ATFX)
           H(ind, :) = H(ind, :) + ATFX{j}*sum(dot(ATFY{j},Ytilde{t}));           
       end
    end
    % Assemble Q matrix
    Q = zeros(N*n,N*n);    
    for i = 1:N        
        indc = (i-1)*n+1:i*n;
        for j = 1:length(ATAX)
            TEMP = ATAY{j}*Ytilde{i};
            for t = 1:N
                indr = (t-1)*n+1:t*n;
                Q(indr,indc) = Q(indr,indc) + ATAX{j}*sum(dot(TEMP,Ytilde{t}));
            end
        end
    end
    % Solve for unknowns
     if (opts.symmetric && N == 1)
        XSOL = lyap(Q, -(H+H'));
    else
        XSOL = Q\H;
     end  
    for t = 1:N
       X{t} = XSOL((t-1)*n+1:t*n, :); 
    end   
    
    % Solve for Y's
    % Assemble H matrix
    H = zeros(N*n,n);
    for t = 1:N
       ind = (t-1)*n+1:t*n;
       for j = 1:length(ATFY)
           H(ind, :) = H(ind, :) + ATFY{j}*sum(dot(ATFX{j},X{t}));           
       end
    end
    % Assemble Q matrix
    Q = zeros(N*n,N*n);    
    for i = 1:N        
        indc = (i-1)*n+1:i*n;
        for j = 1:length(ATAY)
            TEMP = ATAX{j}*X{i};
            for t = 1:N
                indr = (t-1)*n+1:t*n;
                Q(indr,indc) = Q(indr,indc) + ATAY{j}*sum(dot(TEMP,X{t}));
            end
        end
    end
    % Solve for unknowns
    if (opts.symmetric && N == 1)
        YSOL = lyap(Q, -(H+H'));
    else
        YSOL = Q\H;
    end
    for t = 1:N
       Y{t} = YSOL((t-1)*n+1:t*n, :); 
    end   
       
    % Normalize each Kronecker pair (including acceleration
    if (opts.normalize)
        for i = 1:N
           ratio = sqrt(norm(X{i},'fro')/norm(Y{i},'fro'));
           X{i} = X{i}/ratio;
           Y{i} = Y{i}*ratio;
           Ytilde{i} = Ytilde{i}*ratio;
        end
    end
    
    % Calculate solution as a sum of X_i kron Y_i
    if (normFlag)
        % display objective value  
        oldNorm = newNorm;
        newNorm = evalObj(F,B,C,X,Y);
        relObj = abs(newNorm - oldNorm)/newNorm;
        res(iter) = newNorm;
        if (opts.showInfo)
            fprintf('\tobjective value: %f\n',newNorm);
        end
        if iter > 1
            if res(iter) > res(iter-1)
               alpha = 1;
               iter = iter - 1;
            end
        end
    end
    
    % Acceleration
    if (opts.useFast)
        oalpha = alpha;
        alpha = (1 + sqrt(4*alpha^2+1))/2;
        if (opts.fastRestart > 0)
            if (mod(iter,opts.fastRestart) == 0)
                oalpha = 1;
                alpha = 1;
            end
        end
        for i = 1:N
            Ytilde{i} = Y{i} + (oalpha-1)*(Y{i}-OY{i})/alpha;
        end
    else
        for i = 1:N
            Ytilde{i} = Y{i};
        end
    end
    
    
  
    
    
    % Evaluate relative change in solution
    relSol = 0;
    for i = 1:N
        relSol = max(relSol,  norm(X{i} - OX{i},'fro')/ norm(X{i},'fro'));
        relSol = max(relSol,  norm(Y{i} - OY{i},'fro')/ norm(Y{i},'fro'));
    end   
    relchange(iter) = relSol;
    if (opts.showInfo)
        fprintf('\trelative change: %f\n',relSol);
    end

    
    loopCondition = (relSol > opts.relSol) && (iter < opts.maxIter);
    if (~isempty(opts.relObj))
        loopCondition = loopCondition && (relObj > opts.relObj);
    end
end

iterNum = iter;
relchange = relchange(1:iterNum);
if (nargout >= 5)
    res = res(1:iterNum);
end


end

