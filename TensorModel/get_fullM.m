function MAT = get_fullM(M,ii,c)
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

[~,~,d] = size(M);
N = size(M{1},1);
for i = 2:d
    N = N*size(M{ii,1,i},1);
end
MAT = zeros(N);
for j = 1:c
    MAT = MAT+kron({M{ii,j,:}});
end

end
