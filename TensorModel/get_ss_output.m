function [X,Y] = get_ss_output(n,A,B,C,U)
% Computes the output of a tensor state-space model using n-mode products
% 
% INPUT:
% n: row vector 1xd (d is the tensor order), order for each factor matrix in A
% A: cell dx1,
% B: cell dx1,
% C: cell dx1, 
% U: cell dx1, 
% OUTPUT:
% X: cell Ntx1 (Nt is the number of temporal samples in the dataset), state
% Y: cell Ntx1, output
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = length(A);
Nt = length(U);
X = cell(Nt,1); 
Y = cell(Nt,1); 
X{1} = zeros(fliplr(n));
for k = 1:Nt
	X{k+1} = tmprod(X{k},A,[d:-1:1])+tmprod(U{k},B,[d:-1:1]);
	Y{k} = tmprod(X{k},C,[d:-1:1]);
end

end
