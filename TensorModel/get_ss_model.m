function [A,B,C,M,a] = get_ss_model(d,n,I,J,th,s,opt)
% Generate tensor state-space model
%
% INPUT:
% d: scalar, tensor order
% n: row vector 1xd (d is the tensor order), order for each factor matrix in A
% I: row vector of dimension equal to the tensor order, contains the dimensions of the input grid
% J: row vector of dimension equal to the tensor order, contains the dimensions of the output grid
% th: scalar, threshold >1 to ensure the spectral radius of each factor matrix in A is Schur stable
% s: length of FIR filter
% opt: 
% opt.pos: scalar. if set to 1, the factor matrices are strictly positive elementwise
%	   else, random matrices
% OUTPUT:
% A: cell dx1
% B: cell dx1
% C: cell dx1
% a: scalar, spectral radius of Kronecker matrix
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

if exist('opt','var') && isfield(opt,'pos'), pos = opt.pos; else pos = 0; end
	
% Generate system
A = cell(d,1);
B = cell(d,1);
C = cell(d,1);
for i = 1:d
    % state transition matrix
    A{i} = randn(n(i));       
    if pos == 1
       A{i} = abs(A{i});
    end	
    while max(abs(eig(A{i}))) > 0.99
        A{i} = A{i}/th;
    end
    % input- and output- matrices
    B{i} = randn(n(i),I(i));
    C{i} = randn(J(i),n(i)); 
    if pos == 1
       B{i} = abs(B{i}); 
       C{i} = abs(C{i}); 
    end
end
a = spectral_radius(A);

M = cell(s,1,d);
for x = 1:d
    for i = 1:s
        M{i,1,x} = C{x}*A{x}^(i-1)*B{x};           
    end
end

end
