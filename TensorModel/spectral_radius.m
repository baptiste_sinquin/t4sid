function a = spectral_radius(A)
% Computes the spectral radius for the Kronecker matrix A
% 
% INPUT:
% A: cell dx1 (d is the tensor order),
% OUTPUT:
% a: spectral radius for the matrix kron(A1,...,Ad)
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = size(A);
a = max(abs(eig(A{1})));
for x = 2:d
    a = a*max(abs(eig(A{x})));
end

end
