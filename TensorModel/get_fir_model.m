function [M] = get_fir_model(d,r,s,I,J,opt)
% Generate factor coefficient-matrices
% 
% INPUT:
% d: tensor order
% r: Kronecker rank
% s: temporal order
% I: row vector 1xd, contains the dimensions of the input grid
% J: row vector 1xd, contains the dimensions of the output grid
% opt may contain: 
% opt.pos: scalar. if set to 1, the factor matrices are strictly positive elementwise
%	   else, random matrices
% opt.sss: scalar. if set to 1, the factor matrices have a Sequentially Semi-Separable structure 
% OUTPUT
% M: cell of size s x r x d, which contains the factor matrices
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

if exist('opt','var') && isfield(opt,'pos'), pos = opt.pos; else pos = 0; end
if exist('opt','var') && isfield(opt,'sss'), sss = opt.sss; else sss = 0; end
	
M = cell(s,r,d);
for x = 1:d
    for j = 1:r
        for i = 1:s
            M{i,j,x} = randn(J(d-x+1),I(d-x+1));
            if pos == 1
            	M{i,j,x} = abs(M{i,j,x});
            end	
            if sss == 1
            	% requires I(i) = J(i), for all i = 1..d
            	p = 1;
            	m = 1;
            	n = 2;
            	N = I(x);
            	SSS = inventSSS(N,p,m,n);
            	M{i,j,x} = construct_new_SSS(SSS);
            end
        end
    end
end

end
