function [U,X,Y] = get_ss_data(Nt,n,A,B,C,I,J,snr)
% Generate input-output data with zero initial conditions
%
% INPUT:
% Nt: scalar, number of temporal samples in the dataset
% n: row vector 1xd (d is the tensor order), order for each factor matrix in A
% A: cell dx1, each matrix of size n(x) x n(x)
% B: cell dx1, each matrix of size n(x) x I(x)
% C: cell dx1, each matrix of size J(x) x n(x)
% I: row vector of dimension equal to the tensor order, contains the dimensions of the input grid
% J: row vector of dimension equal to the tensor order, contains the dimensions of the output grid
% snr: scalar, signal to noise ratio
% OUTPUT:
% U: cell Ntx1, input
% X: cell Ntx1 state
% Y: cell Ntx1, output
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = length(I);
U = cell(Nt,1); 
X = cell(Nt,1); 
Y = cell(Nt,1); 
X{1} = zeros(fliplr(n));
for k = 1:Nt
	U{k} = randn(fliplr(I));
	% using mode-n products
	X{k+1} = tmprod(X{k},A,d:-1:1)+tmprod(U{k},B,d:-1:1);
	Y{k} = tmprod(X{k},C,d:-1:1);
	temp = awgn(vec(Y{k}),snr);
    Y{k} = reshape(temp,fliplr(J));
end

end
