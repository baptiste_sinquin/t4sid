% QUARKS: Identification of multi-dimensionals autoregressive models with
% sums-of-Kronecker parametrization
%
% Only implemented for I = J
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

clc; clear;
close all;

% generate the model 
I = [5 4 2];        % output dimensions
J = I;              % input dimensions
d = length(I);      % tensor order
r = 2;              % Kronecker rank
s = 2;              % length of FIR filter
opt.pos = 0;
opt.sss = 0;
M = get_fir_model(d,r,s,I,J); 

% generate input-output data
Nt = 4e3;
Nid = floor(2/3*Nt);
Nval = Nt-Nid;
snr = 20;
[Uid,Yid] = get_fir_data(I,J,Nid,snr,M);
[Uval,Yval] = get_fir_data(I,J,Nval,snr,M);

% estimate a quarks-rls model
forgetting_factor = 1;
tic
[M_hat,iter,cv] = ho_rls(fliplr(J),fliplr(I),Uid,Yid,M,forgetting_factor);
time = toc;
figure,semilogy(iter(1:Nid-1,2),'Linewidth',0.5)               
ylabel('Relative RMSE for the output'),xlabel('Time sample'),grid on, drawnow
figure,semilogy(cv(1:Nid-1,1:d)),
ylabel('Frobenius norm between two consecutive updates'),xlabel('Time sample'),grid on,drawnow

% check on validation data
Yhat_val = get_fir_output(J,M_hat,Uval);
VAF = compute_vaf_per_channel(Yval,Yhat_val,J);
fprintf('QUARKS-RLS. VAF: %6.2f Time(s): %6.2f \n',VAF,time);



