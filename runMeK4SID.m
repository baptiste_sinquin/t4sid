% K4SID: Kronecker Structured large-Scale SubSpace IDentification
% 
% Identification of multi-dimensional deterministic state-space models 
% with Kronecker parametrization. The stochastic case is handled using 
% replacing the input with the output. It is assumed I_i = J_i, for all 
% i = 1..d. In this code, differents methods are proposed for estimating
% - recursive or batch-wise identification of the factored Markov
% parameters using the QUARKS
% - estimation of an ambiguity parameter solving a multilinear low-rank
% optimization via either Block-Coordinate Update (BCU) with slack variables, 
% or a convex relaxation using a nuclear norm regularization (NN). Both 
% methods require a regularization parameter whose initial value matters: 
% it may be fixed (or given in a specified range) for a given size. Quicker
% algorithms require to have some knowledge of the hyperparameters used 
% especially in the second step. It is part of ongoing work to find methods 
% that alleviate the computations. For now, we carry out these computations 
% in parallel.
% - The state-space matrices are then estimated from a SVD on the block-Hankel 
% matrices.
% 
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

clc; clear;
close all;

plot_cv = 0;            % 1 for plotting convergence in the QUARKS/RLS
r = 1;                  % Kronecker rank, fixed to 1
I = [6 5 6];            % dimensions of the input data (total : prod(I))
J = I;                  % dimensions of the output data (total : prod(J))
n = J+1;                % order of factor matrix A_i, arbitrary here > J
d = length(I);          % tensor order 
threshold = 1.05;       % >1, for ensuring stability of the system
%
method = 'bcu';         % method chosen for the second step: 'bcu' or 'nn'
Mc = 10;                % number of Monte-Carlo simulations per tensor order
m = 1;
% for Mc Monte-Carlo runs with different random generations of state-space
% matrices
while m <= Mc           
    fprintf('Generate new Kronecker model, %i out of %i.\n',m,Mc);
    
    %% generate the model and the input-output data
    s = 15;             % odd, length of FIR filter
    s1 = (s+1)/2;
    [A,B,C,M,a] = get_ss_model(d,n,I,J,threshold,s); 
    data.system(m,1) = a;
    
    % generate input-output data on Nid temporal samples
    Nt = floor(10*d*s*max(J)*3/2);
    Nid = floor(2/3*Nt);
    Nval = Nt-Nid;
    snr = 30;
    [Uid,~,Yid] = get_ss_data(Nid,n,A,B,C,I,J,snr);
    [Uval,~,Yval] = get_ss_data(Nval,n,A,B,C,I,J,snr);
    
    %% estimate a quarks model
    quarks = 1;         % with batch-wise method
    rls = 0;            % with recursive method (quicker but less accurate)
    if quarks == 1 
        tic
        [M_hat,norm_residual] = ho_quarks(I,J,s,r,d,Uid,Yid);
        data.quarks(m,1) = toc;
        if plot_cv == 1
            figure,semilogy(norm_residual),grid on,
            xlabel('Numer of ALS iterations'),ylabel('Residual least squares for ALS'),drawnow
        end
        data.quarks(m,2) = norm_residual(find(norm_residual>0,1,'last'));
    elseif rls == 1           
        tic
        forgetting_factor = 1;
        [M_hat,iter,cv] = ho_rls(s,d,J,I,Uid,Yid,M,forgetting_factor);
        data.quarks(m,1) = toc;
        if plot_cv == 1
            figure,semilogy(iter(1:Nid-1,2),'Linewidth',0.5)               
            ylabel('Relative RMSE for the output'),xlabel('Time sample'),grid on, drawnow
            figure,semilogy(cv(1:Nid-1,1:d)),
            ylabel('Frobenius norm between two consecutive updates'),xlabel('Time sample'),grid on,drawnow
            figure,semilogy(cv(1:Nid-1,d+1)),
            xlabel('Time sample'),grid on,drawnow
        end
        data.quarks(m,2) = iter(end,2);
    end
    t = zeros(s,d); 
    for i = 1:s
        for x = 1:d                    
            t(i,x) = pinv(vec(M_hat{i,1,x}))*vec(M{i,1,x});  
        end
    end
    v = prod(t,2);             
    data.quarks(m,3) = norm(v-1)/norm(ones(s,1),2);  
        
    % VAF on validation data
    Yhat_val = get_fir_output(fliplr(J),M_hat,Uval);
    VAF = compute_vaf_per_channel(Yval,Yhat_val,J);
    data.quarks(m,4) = VAF;
    data.quarks(m,5) = s*sum(J.^2); 
    fprintf('VAF (with QUARKS): %6.2f\n',data.quarks(m,4));

    %% estimate a sequence of ambiguity factors t_ij 
    if strcmp(method,'bcu') == 1 
        
        % Method 1: with Block-Coordinate Update (BCU) and slack variables
        [alpha,H_hat,time_bcu] = solve_multilinear_lowrank_via_BCU(M_hat,I,J);
        rangeVAF = []; VAFmax = -1;
        for ii = 1:length(H_hat)                          
            [n_hat,VAF,t4mat,rangeVAF] = evaluate_estimation(H_hat{ii},Uval,Yval,I,J,n,rangeVAF);
            if VAF > VAFmax
                ii_opt = ii;
                VAFmax = VAF;
                nn = mean(n_hat);
                time = time_bcu{ii}+t4mat;
            end
        end
        data.bcu(m,1) = VAFmax;
        data.bcu(m,2) = nn;
        data.bcu(m,3) = time;
        data.bcu(m,4:4+length(rangeVAF)-1) = rangeVAF;
        fprintf('VAF (with BCU): %6.2f\n',max(rangeVAF));
        
        % estimate the state-sequence beforehand
        if d == 2
            alpha = alpha{ii_opt};
            alpha_opt = alpha(:,1);
            beta_opt = alpha(:,2);
            unfolding = fill_tensor(alpha_opt,beta_opt,J,M_hat,Uid,Yid);               
            [U,S,V] = svd(unfolding,'econ');
            %figure,semilogy(diag(S),'x','Linewidth',0.75),grid on,title('Singular value for unfolded matrix')
            store_VAF = zeros(n(1)+10,1);
            for nn = 2:n(1)+5;
                [n_hat,A_hat,B_hat,C_hat] = getX2ABC(nn,U,S,V,J,Uid,s);
                [~,Yhat_val] = get_ss_output(n_hat,A_hat,B_hat,C_hat,Uval);
                VAF = compute_vaf_per_channel(Yval,Yhat_val,J);
                store_VAF(nn,1) = VAF;
            end
            data.bcu(m,4+length(rangeVAF)) = max(store_VAF);
        end 
        
    elseif strcmp(method,'nn') == 1  
        
        % Method 2: estimate with convex relaxation of bilinear constraints
        % using the nuclear norm (NN)
        [H_hat,time_nn] = solve_multilinear_lowrank_via_NN(M_hat,I,J);
        rangeVAF = []; VAFmax = -1;
        for ii = 1:length(H_hat)  
            [n_hat,VAF,t4mat,rangeVAF] = evaluate_estimation(H_hat{ii},Uval,Yval,I,J,n,rangeVAF);
            if VAF > VAFmax
                VAFmax = VAF;
                nn = mean(n_hat);
                time = time_nn{ii}+t4mat;
            end
        end
        data.nn(m,1) = VAFmax;
        data.nn(m,2) = nn;
        data.nn(m,3) = time;
        data.nn(m,4:4+length(rangeVAF)-1) = rangeVAF; 
        fprintf('VAF (Convex Relax): %6.2f\n',max(rangeVAF));
        
    end

    m = m+1;
    
    save('K4SID_MonteCarlo.mat','data')
end



