The toolbox contains Matlab code for identifying the spatial-temporal dynamics of multi-dimensional systems. 

The algorithms are presented in the references: 
Monchen, G., "Recursive Kronecker-based Vector Auto-Regressive identification for large-scale adaptive optics systems", Master thesis, TU Delft, 2017. Online: https://repository.tudelft.nl/islandora/object/uuid%3A530e5665-3568-4063-827a-b2353a70a81a
Monchen, G., Sinquin, B., and Verhaegen, M., "Recursive Kronecker-based Vector Auto-Regressive identification for large-scale Adaptive Optics", Accepted for publication in IEEE Control for Systems Technology, 2018
Sinquin, B., and Verhaegen, M., "QUARKS: Identification of Large-Scale Kronecker Vector-AutoRegressive models", Accepted for publication in IEEE Transactions on Automatic Control, 2018. Preliminary version online: https://arxiv.org/abs/1609.07518
Sinquin, B., and Verhaegen, M., "K4SID: Large-scale subspace identification with Kronecker modeling", Accepted for publication in IEEE Transactions on Automatic Control, 2018. Online: https://ieeexplore.ieee.org/document/8362723/
Sinquin, B., and Verhaegen, M., "Tensor-based predictive control for extremely large-scale adaptive optics", submitted to JOSA A, 2018
Varnai, P., "Exploiting Kronecker structures, with application to optimizations problems arising in the field of adaptive optics", Master thesis, TU Delft, 2017. Online: https://repository.tudelft.nl/islandora/object/uuid%3A98f7cf6e-6ded-4f50-8df7-89944d6a0830

Although some of these papers are not all currently publicly available, they can be provided on request. 

There are five main files. Start by running runMeTutorial with Run And Advance, all subfolders are added to the path while running runMeTutorial.m.
1. runMeTutorial.m
This file presents the basic tensor operations and the multi-level matrices that we deal with. 
2. runMeQUARKS.m
This file identifies a tensor auto-regressive model using batch data. The signal generating model is Finite-Impulse-Response in order to visualize the global convergence of the Alternating Least Squares. 
3. runMeQUARKSRLS.m
This file identifies a tensor auto-regressive model recursively. The signal generating model is Finite-Impulse-Response in order to visualize the global convergence of the algorithm.
4. runMeApproxInverse.m
Performing even standard linear algebra operations such as addition, multiplication or inversion on certain type of matrices may destroy its original structure. Structure-preserving properties matter in deriving 
elegant and scalable algorithms. This file determines whether a matrix written as a sum of few Kronecker terms admits a low-Kronecker inverse (or, at least, be approximated by).
5. runMeK4SID.m
This file identifies state-space models with a Kronecker structure. 

The Matlab code requires the TensorLab toolbox: 
Vervliet N., Debals O., Sorber L., Van Barel M. and De Lathauwer L. Tensorlab 3.0, Available online, Mar. 2016. URL: https://www.tensorlab.net/

The toolbox will be completed during summer 2018 with updates on the state-space identification methods. Constructive suggestions for improvement are always welcome.
The toolbox was written collaboratively by (alphabetical order) Guido Monchen, Baptiste Sinquin, Peter Varnai while studying at the Delft Center for Systems and Control in TU Delft. The research was done under supervision of Prof. M. Verhaegen.

The toolbox can be cited as:
Sinquin B., Varnai P., Monchen G. and Verhaegen M., "Tensor toolbox for identifying multi-dimensional systems", Available online. 

Contact: baptiste.sinquin@gmail.com
