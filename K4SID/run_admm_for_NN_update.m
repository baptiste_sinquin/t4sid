function [xx,residuals] = run_admm_for_NN_update(H_op,H_adj,HH,Ho,M_op,M_adj,MM,Mo,I,J,s,d)
% Run ADMM
% TODO: add stopping criterion following the guidelines in Boyd.
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

% Initialize options
if exist('opt','var') && isfield(opt,'maxiter'), maxiter = opt.maxiter; else maxiter = 1e3; end
if exist('opt','var') && isfield(opt,'t'),       t = opt.t;             else t = 1;        end

% Initialize variables 
s1 = (s+1)/2;
Xh = cell(d,1);
Zh = cell(d,1);
for x = 1:d
    Xh{x} = zeros(s1*J(x),s1*I(x));
    Zh{x} = zeros(s1*J(x),s1*I(x));
end
Xm = cell(d,1);
Zm = cell(d,1);
for i = 1:s-1
    for j = 1:d-1
        Xm{i,j} = zeros(2);
        Zm{i,j} = zeros(2);
    end
end
tempH = cell(d,1);
tempM = cell(s-1,d-1);
%
imat = inv(diag(HH)+MM); 

% ADMM iterations
residuals = zeros(maxiter,d+(s-1)*d-1);
iter = 1;
while iter < maxiter
    
    % Update x    
    for x = 1:d
        tempH{x} = -Zh{x}+t*(Xh{x}-Ho{x});  
    end
    for i = 1:s-1
        for j = 1:d-1
            tempM{i,j} = -Zm{i,j}+t*(Xm{i,j}-Mo{i,j});
        end
    end
    vec = H_adj(tempH)+M_adj(tempM);
    xx = imat*vec;
    Hx = H_op(xx);
    Mx = M_op(xx);

    % Update X
    id = 1;
    for x = 1:d
        [U,S,V] = svd(Hx{x}+Ho{x}+Zh{x}/t); 
        Xh{x} = U*max(0,S-1/t)*V';
        residuals(iter,id) = norm(Xh{x}-Hx{x}-Ho{x},'fro')/norm(Hx{x}+Ho{x},'fro');
        id = id+1;
    end
    for i = 1:s-1
        for j = 1:d-1
            [U,S,V] = svd(Mx{i,j}+Mo{i,j}+Zm{i,j}/t); 
            Xm{i,j} = U*max(0,S-1/t)*V';
            residuals(iter,id) = norm(Xm{i,j}-Mx{i,j}-Mo{i,j},'fro')/norm(Mx{i,j}+Mo{i,j},'fro');
            id = id+1;
        end
    end

    % Update duals
    for x = 1:d
        Zh{x} = Zh{x} + t*(Hx{x}+Ho{x}-Xh{x});
    end
    for i = 1:s-1
        for j = 1:d-1
            Zm{i,j} = Zm{i,j} + t*(Mx{i,j}+Mo{i,j}-Xm{i,j});
        end
    end

    iter = iter+1;
end

