function [ALPHA,H_HAT,TIME] = solve_multilinear_lowrank_via_BCU(M_hat,I,J,opt)
% Estimate an ambiguity sequence of scalars such that H_op(x)+Ho{x} is low
% rank and allows to realize the factored state-space matrices 
%
% INPUT
% M_hat: cell sxrxd containing the estimated factor parameters 
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
% opt:
% opt.random: 0 if initial guesses set to 1, 1 for random init
%
% OUTPUT
% ALPHA: set of optimized variables, one for each regularization parameter
% TIME: optimization time, one for each regularization parameter
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

if exist('opt','var') && isfield(opt,'random'), random = opt.random; else random = 0; end
if exist('opt','var') && isfield(opt,'debug'), debug = opt.debug; else debug = 0; end

[s,~,d] = size(M_hat);

muRange = logspace(0,2.5,6);
maxiter = 30;
sub_iterations = 4;

% quadratic norm on slack variable via \| Qx \|_2
Q = zeros(s,2*s);    
Q(:,s+1:2*s) = eye(s);
bb = ones(s,1);
            
H_HAT = cell(length(muRange),1);
TIME = cell(length(muRange),1);
parfor ii = 1:length(muRange)
    tic
    mu = muRange(ii);
    % init
    if random == 0
        alpha = ones(s,d);
    else
        alpha = randn(s,d); 
    end
    %
    iter = 1;
    cost = Inf;
    while iter<maxiter && cost>5e-3
        for x = 1:d		    
            % Ax = b: relaxed bilinear constraint 
            if d > 2
                AA = [diag(prod(alpha(:,[1:x-1 x+1:d])')) -eye(s)];
            elseif d == 2
                if x == 1
                    AA = [diag(alpha(:,2)) -eye(s)];
                elseif x == 2
                    AA = [diag(alpha(:,1)) -eye(s)];
                end
            end
            % Related to low-rank reduced-size systems
            % i.e with the operator H s.t H(x) = X, block Hankel
            H_op = @(xx) call_L_op(xx,{M_hat{:,1,x}},J(x),I(x));
            H_opa = @(ZZ) call_L_op_adj(ZZ,{M_hat{:,1,x}},J(x),I(x));
            HH = compute_GramL({M_hat{:,1,x}},J(x),I(x));
            % Run ADMM 
            [xx,residuals] = run_admm_for_BCU_update(mu,Q,AA,bb,H_op,H_opa,HH,s,J(x),I(x));
            %figure,semilogy(residuals)
            %legend('nuclear norm consensus','eq. constraint')
            %drawnow
            %criteria = norm(AA*xx-bb,2)/norm(bb);
            %fprintf('Bilinear constraint: %6.2f\n',criteria);
            alpha(:,x) = xx(1:s);
        end
        % Update mu
        if mod(iter,sub_iterations) == 0
           mu = mu*5;
        end
        % evolution of the residual
        cost = norm(prod(alpha')-1,2)/norm(ones(s,1),'fro');
        % 
        iter = iter+1; 
    end
    TIME{ii} = toc;
    ALPHA{ii} = alpha;
    H_HAT{ii} = cell(d,1);
    for x = 1:d
         H_HAT{ii}{x} = call_L_op(alpha(:,x),{M_hat{:,1,x}},J(x),I(x));
    end   
end


end




