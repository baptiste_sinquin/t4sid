function x = call_L_op_adj(Z,M_hat,J,I)
% Compute adjoint of linear operator L_op
% 
% INPUT: 
% Z: cell dx1 containing block-Hankel matrices of size s1*J(x) x s1*I(x)
% M_hat: cell sxrxd containing the estimated factor parameters 
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
% OUTPUT: 
% xx: vector 2*(d-1)*(s-1) containing the variables
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

s = length(M_hat);
s1 = (s+1)/2;
x = zeros(s,1);
%
ZZ = Z(:,1:I);
for i = 1:s1-1
   ZZ = [ZZ;Z(J*(s1-1)+1:J*s1,i*I+1:(i+1)*I)]; 
end
%
for i = 1:s
    x(i) = reshape(M_hat{i},1,J*I)*reshape(ZZ((i-1)*J+1:i*J,:),J*I,1);
end
tmp = [1:s1]';
scale = [tmp;flipud(tmp(1:end-1))];
x(1:s) = x(1:s).*scale;
x(s+1:2*s) = zeros(s,1);

end


