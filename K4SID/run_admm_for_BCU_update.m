function [xx, residuals] = run_admm_for_BCU_update(mu,Q,A,b,H_op,H_opa,HH,s,J,I)
% Run ADMM
% TODO: add stopping criterion following the guidelines in Boyd.
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

% Initialize options
if exist('opt','var') && isfield(opt,'maxiter'), maxiter = opt.maxiter; else maxiter = 1e3; end
if exist('opt','var') && isfield(opt,'t'),       t = opt.t;             else t = 1;        end

% Initialize variables 
s1 = (s+1)/2;
rx = s1*J;
cx = s1*I;
s = size(b,1);
X = zeros(rx,cx);
y2 = zeros(s,1); 
Z = zeros(rx,cx);

% ADMM iterations
iter = 1;
residuals = zeros(maxiter,2);
while iter < maxiter
    % Update x
    mat = 2*mu*Q'*Q+t*(A'*A+HH); 
    vec = A'*(-y2+t*b)+H_opa(-Z+t*X);
    xx = mat\vec;
    Hx = H_op(xx);

    % Update X
    [U,S,V] = svd(Hx+Z/t); 
    X = U*max(0,S-1/t)*V';

    residuals(iter,:) = [norm(X-Hx,'fro')/norm(Hx,'fro') norm(A*xx-b,2)/norm(b,2)];

    % Update duals
    y2 = y2 + t*(A*xx-b);
    Z = Z + t*(Hx-X);
    % 
    iter = iter+1;
end

