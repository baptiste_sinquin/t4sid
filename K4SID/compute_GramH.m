function M = compute_GramH(M_hat,J,I)
% Gramian matrix relative to the linear operator H_op
% 
% INPUT
% M_hat: cell sxrxd containing the estimated factor parameters 
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
% 
% OUTPUT
% M: Gramian matrix
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

[s,~,d] = size(M_hat);
s1 = (s+1)/2;
nx = (s-1)*(2*d-2);
id = sparse(eye(nx));
M = zeros(nx);
for x = 1:d
    A = sparse(s1^2*J(x)*I(x),nx);
    for i = 1:s-1
        ii = (s-1)*(x-1)+i;
        temp = call_H_op(id(:,ii),M_hat,J,I); 
        xx = temp{x}(:);    
        A(:,ii) = xx;
    end
    M = M+A'*A;
end

end
