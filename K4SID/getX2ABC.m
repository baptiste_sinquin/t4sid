function [n_HAT,A_HAT,B_HAT,C_HAT] = getX2ABC(nn,U,S,V,J,Uid,s)
% Case d = 2 only. Estimates the factor matrices from the state-sequence.
% 
% INPUT
% nn : system order (fixed)
% Uu1, Vu1, Su1:  left, right singular vectors and singular values for the first unfolding of the three
% dimensional-tensor
% J: vector 1xd, dimension for output array
% Uid: cell Nid x 1, containing the input data used for identification
% s: length of FIR filter
%
% OUTPUT
% n_hat: vector of length d, containing the system orders
% A_hat: cell dx1, containing matrices of size n_hat(x) x n_hat(x)
% B_hat: cell dx1, containing matrices of size n_hat(x) x I(x)
% C_hat: cell dx1, containing matrices of size J(x) x n_hat(x)
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

d = 2;
Nid = length(Uid);
Mt = Nid-s+1;
s1 = (s+1)/2;
phi = s1-1;
%
n1 = nn;
n2 = nn;
Uu_n = U(:,1:nn);
C2_hat = Uu_n(1:J(2),:);
A2_hat = get_A_stable(Uu_n,J(2));
% Re-shuffle the right singular vectors
Vt = S*V';
Vt_n = Vt(1:nn,:);
H = zeros(J(1)*phi,n2*Mt);
idx = 1;
for i = 1:phi
    for j = 1:Mt
        H((i-1)*J(1)+1:i*J(1),(j-1)*n2+1:j*n2) = Vt_n(:,(idx-1)*J(1)+1:idx*J(1))';
        idx = idx+1;
    end
end
[Uu2,Su2,Vu2] = svd(H,'econ');
%figure,semilogy(diag(Su2),'+','Linewidth',1.5),title('Singular value for reshaped right singular value matrix')
%logsv = log(diag(Su2));
%n1 = min(n_end,find(logsv>(max(logsv)+min(logsv))/2,1,'last'))/1.5;
Uu_n = Uu2(:,1:nn);
C1_hat = Uu_n(1:J(1),:);
A1_hat = get_A_stable(Uu_n,J(1));
% Estimate the state-sequence
Vt = Su2*Vu2';
Vt_n = Vt(1:nn,:);
XX_hat = zeros(nn,nn,Mt);
for k = 1:Mt
    XX_hat(:,:,k) = reshape(Vt_n(:,(k-1)*n2+1:k*n2),n1,n2)';
end
% we now have estimated A1, A2, C1, C2, X; it remains B1 and B2
% we solve a bilinear least-squares to estimate the Kronecker generators from the state-sequence
Nt = length(XX_hat);
OutputX = zeros(n2,n1*(Nt-1)); 
OutputXT = zeros(n1,n2*(Nt-1));
for k = 2:Nt
    OutputX(:,(k-2)*n1+1:(k-1)*n1) = XX_hat(:,:,k)-A2_hat*XX_hat(:,:,k-1)*A1_hat';
    OutputXT(:,(k-2)*n2+1:(k-1)*n2) = OutputX(:,(k-2)*n1+1:(k-1)*n1)';
end  
itermax = 10;
B1_hat = randn(n1,J(1));    
for iterALS1 = 1:itermax
    InputX = zeros(J(2),n1*(Nt-1));
    for k = 1:Nt-1
        InputX(:,(k-1)*n1+1:k*n1) = Uid{phi+1+k}*B1_hat';
    end
    B2_hat = OutputX*pinv(InputX);
    %
    InputX = zeros(J(1),n2*(Nt-1));
    for k = 1:Nt-1
        InputX(:,(k-1)*n2+1:k*n2) = Uid{phi+1+k}'*B2_hat';
    end
    B1_hat = OutputXT*pinv(InputX);
end
n_HAT = nn*ones(1,d);
A_HAT{1} = A1_hat; A_HAT{2} = A2_hat; 
B_HAT{1} = B1_hat; B_HAT{2} = B2_hat;
C_HAT{1} = C1_hat; C_HAT{2} = C2_hat;

end