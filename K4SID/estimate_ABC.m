function [n_hat,A_hat,B_hat,C_hat] = estimate_ABC(mat,I,J,varargin)
% Estimate the system order and the system matrices
% 
% INPUT
% mat
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
% s: length of FIR filter
% varargin: may contain prior knowledge of the order n_hat (contained
% within a vector of length d)
% 
% OUTPUT
% n_hat: vector of length d, containing the order of the factor matrices
% A_hat: cell dx1, containing the matrices of size n_hat(x) x n_hat(x)
% B_hat: cell dx1, containing the matrices of size n_hat(x) x I(x)
% C_hat: cell dx1, containing the matrices of size J(x) x n_hat(x)
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

d = length(I);
if isempty(varargin) 
    n_hat = zeros(1,d);
else
    n_hat = varargin{1}*ones(1,d);
end
A_hat = cell(d,1);            
B_hat = cell(d,1);
C_hat = cell(d,1);
%
for x = 1:d
    id = 1;
    % standard realization via the block-Hankel matrix
    [U,S,V] = svd(mat{x}); 
    sv = diag(S);
    %figure,semilogy(sv,'+')
    if sv(1) > 0
        id = 0;   
        if isempty(varargin)            
            n_hat(x) = find(sv/max(sv)>0.05,1,'last');
            %logsv = log(sv);
            %n_hat(x) = find(logsv>(max(logsv)+min(logsv))/2,1,'last');
        end
        C_hat{x} = U(1:J(x),1:n_hat(x));
        Un = U(:,1:n_hat(x));
        A_hat{x} = get_A_stable(Un,J(x));
        % discard aberrant values
        if sum(vec(isnan(A_hat{x}))) > 0 || sum(vec(isinf(A_hat{x}))) > 0
            A_hat{x} = zeros(n_hat(x));
            id = 1;
        end
        Vn = S(1:n_hat(x),1:n_hat(x))*V(:,1:n_hat(x))';
        B_hat{x} = Vn(:,1:I(x));
    end
    if id == 1
        A_hat{x} = 0;
        B_hat{x} = zeros(1,I(x));
        C_hat{x} = zeros(J(x),1);
        n_hat = ones(1,d);
    end
end

end
