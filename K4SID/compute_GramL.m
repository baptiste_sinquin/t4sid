function M = compute_GramL(M_hat,J,I)
% Compute Gramian matrix associated with linear operator L
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

s = length(M_hat);
s1 = (s+1)/2;
nx = 2*length(M_hat);
A = sparse(s1^2*J*I,nx);
id = sparse(eye(nx));
for ii = 1:nx   
    temp = call_L_op(id(:,ii),M_hat,J,I); 
    A(:,ii) = temp(:);
end
M = A'*A;

end
