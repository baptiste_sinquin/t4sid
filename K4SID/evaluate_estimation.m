function [n_mean,VAF,time,maxVAF] = evaluate_estimation(H_hat,Uval,Yval,I,J,n,maxVAF)
% Estimate the state-space matrices and evaluate the quality of the 
% estimation on validation data. 
%
% INPUT
% H_hat
% Uval
% Yval
% I: vector 1xd, dimension for input array
% J: vector 1xd, dimension for output array
% s: length of FIR filter
% n: true system order
% maxVAF: range of VAF values for different system orders. for each system
% order, the maximum is stored for all initialition parameters of the
% low-rank bilinear algorithm
% 
% OUTPUT
% n_mean: mean of the selected system order
% VAF: Variance Accounted For
% time: time for realizing the system matrices from a block-Hankel matrix
% maxVAF: range of VAF values for different system orders.
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

% with order estimation
tic
[n_hat,A_hat,B_hat,C_hat] = estimate_ABC(H_hat,I,J);   
time = toc;
n_mean = mean(n_hat);
[~,Yhat_val] = get_ss_output(n_hat,A_hat,B_hat,C_hat,Uval);
VAF = compute_vaf_per_channel(Yval,Yhat_val,J);

% or, looping over the order
store_vaf = zeros(6,1);
for nh = n(1):n(1)+8
    [n_temp,A_hat,B_hat,C_hat] = estimate_ABC(H_hat,I,J,nh);
    [~,Yhat_val] = get_ss_output(n_temp,A_hat,B_hat,C_hat,Uval);
    VAF = compute_vaf_per_channel(Yval,Yhat_val,J);
    store_vaf(nh-n(1)+1) = VAF;
end
if isempty(maxVAF) == 1
    maxVAF = store_vaf;
else
    maxVAF = max(store_vaf,maxVAF);
end                    
               

end