# Utils
# G. Monchen (2017)
# Edit: B.Sinquin (May 2017)

import sys
from numpy import *

def generateVARXData(cRows, cColumns, cIterations, cTempOrder, cKronRank, cBW):
	
	# Variance of random matrices
	cR = 1
	# SNR of noise when generating identification data
	cSNR = 20
	
	# The Kronecker factor matrices
	Creal = zeros((cRows,cRows,cTempOrder,cKronRank))
	Breal = zeros((cColumns,cColumns,cTempOrder,cKronRank))
	if cBW == 0:
		for j in range(0,cTempOrder):	
			for i in range(0,cKronRank):
				Creal[:,:,j,i] = cR * random.randn(cRows, cRows)
				Breal[:,:,j,i] = cR * random.randn(cColumns, cColumns)
	elif cBW>0:
		# If a bandwidth is defined, construct the matrices differently
		# TODO
		if cBW > 0:
			Creal[:,:,0,0] = diag(cR * random.randn(cRows),0)
			Breal[:,:,0,0] = diag(cR * random.randn(cRows),0)
		
			t = math.floor(cBW/2)		
			if cBW > 1:
				for i in range(1,int(t)+1):
					Creal[:,:,0,0] = Creal[:,:,0,0] + diag(cR * random.randn(cRows-i),i) + diag(cR * random.randn(cRows-i),-i)
					Breal[:,:,0,0] = Breal[:,:,0,0] + diag(cR * random.randn(cRows-i),i) + diag(cR * random.randn(cRows-i),-i)
	
	# generate random input signal
	uIdent = random.randn(cRows * cColumns, cIterations)
	
	# Write coefficient-matrices
	Areal = zeros((cRows*cColumns,cRows*cColumns,cTempOrder))
	for j in range(0,cTempOrder):
		for i in range(0,cKronRank):
			Areal[:,:,j] += kron(Breal[:,:,j,i],Creal[:,:,j,i])
	
	# output signal
	yIdent = zeros((cRows * cColumns, cIterations))
	
	# Generate output data
	for k in range(cTempOrder,cIterations):
		for j in range(0,cTempOrder):
			yIdent[:,k] += dot(Areal[:,:,j], uIdent[:,k-j])
			
	# Return the input and output data
	return yIdent, uIdent, Areal
	
def computeVAF(A, leftMat, rightMat, cRows, cColumns, cTempOrder, cRankKron ):
	Ahat = zeros((cRows*cColumns,cRows*cColumns,cTempOrder)) 
	Bhat = zeros((cColumns,cColumns,cTempOrder,cRankKron))
	Chat = zeros((cRows,cRows,cTempOrder,cRankKron))
	print('\n ALS: VAF for coefficient-matrices')
	for j in range(0,cTempOrder):
		Ahat[:,:,j] = zeros((cRows*cColumns,cRows*cColumns))	
		for i in range(0,cRankKron):
			Chat[:,:,j,i] = leftMat[j,i,:,:]
			Bhat[:,:,j,i] = rightMat[j,i,:,:]
			Ahat[:,:,j] += kron(Bhat[:,:,j,i].T,Chat[:,:,j,i].T)
		Arealvec = reshape(A[:,:,j], (cRows*cColumns)**2, 'F')
		Atestvec = reshape(Ahat[:,:,j], (cRows*cColumns)**2, 'F')
		vaf = 100 * (1 - (cov(Arealvec-Atestvec)/cov(Arealvec)))
		print(vaf)

	
if __name__ == "__main__":
	# Number of inputs
	cRows = 4
	cColumns = 4
	# Spatial order
	cKronRank = 2
	# Temporal order
	cTempOrder = 2
	# Number of time samples
	cIterations = 5	
	
	yIdent, uIdent, Areal = generateVARXData(cRows, cColumns, cIterations, cTempOrder, cKronRank, 0)

