/**
 * HelperFunctions.cuh
 *
 * Helpful utility functions.
 *
 * G. Monchen (2017)
 */

#include <stdio.h>
#include <math.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <curand.h>
#include <time.h>

#ifndef HELPERFUNCTIONS_H
#define HELPERFUNCTIONS_H

#define BLOCK_DIM 32

#define TILE_WIDTH 16

#define cublascall(call) \
	do { \
		cublasStatus_t status = (call); \
		if(CUBLAS_STATUS_SUCCESS != status) { \
			fprintf(stderr, "CUBLAS Error:\n File = %s \n Line = %d \n Code = %d\n", __FILE__,__LINE__, status); \
			cudaDeviceReset(); \
			exit(EXIT_FAILURE); \
		} \
	} while(0)
	
#define cudacall(call) \
	do { \
		cudaError_t err = (call); \
		if(cudaSuccess != err) { \
			fprintf(stderr, "CUDA Error:\n File = %s \n Line = %d \n Code = %s\n", __FILE__,__LINE__, cudaGetErrorString(err)); \
			cudaDeviceReset(); \
			exit(EXIT_FAILURE); \
		} \
	} while(0)

/**
 * Loads a text file into host memory in row-major order.
 * Please note that this function will read a matrix in row-major order, whereas CUDA will
 * use the matrices in column-major order. To make sure that the file is read correctly and
 * the matrix has the desired shape, it is best to vectorize your matrix and then save it to
 * a text file. This way, you make sure that it has the correct shape when you consider that
 * ALS and KrRLS functions treat the matrices in column-major order.
 *
 * @param dest		Pointer to the host memory location where the matrix should be stored.
 * @param location	String containing the location of the text file
 */
void loadMatrixFile(float *dest, const char *location) {
	FILE *f;
	f = fopen(location,"r");
	int i = 0;
	float num;
	
	while(fscanf(f,"%f", &num) != EOF) {
		dest[i++] = num;
	}
	
	fclose(f);
}

/**
 * Writes a matrix in host memory to a text file in column-major order.
 *
 * @param src 		Pointer to the host memory location that contains the matrix
 * @param nRows		Number of rows of the matrix
 * @param nColumns	Number of columns of the matrix
 * @param location	String containing the location of the text file
 */
void writeMatrixFile(float *src, int nRows, int nCols, const char *location) {
	FILE *f = fopen(location, "w");
	
	for(int i = 0; i < nRows; i++) {
		for(int j = 0; j < nCols; j++) {
			fprintf(f, "%f ", src[j * nRows + i]);
		}
		fprintf(f, "\r\n");
	}
	
	fclose(f);
}

/**
 * Fills a matrix with random values from a normal distribution on the GPU.
 *
 * @param dest 	Pointer to the device memory location that is used to store the matrix
 * @param nVals	Number of values to generate, for a matrix this would be nRows * nColumns
 */
void GPUFillRand(float *dest, int nVals) {
	
	// Create the generator
	curandGenerator_t prng;
	curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT);
	
	// Set the seed
	curandSetPseudoRandomGeneratorSeed(prng, (unsigned long long) clock());
	
	// Fill the array
	curandGenerateUniform(prng, dest, nVals);
	
	//printf("%d\n", nVals);
}

/**
 * Calculates the time difference between two timespecs
 *
 * @param start Start time
 * @param end 	End time
 */
timespec diff(timespec start, timespec end)
{
	timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}

/**
 * Constructs the identity matrix on GPU.
 * Can be used by kernel.
 *
 * @param matrix 	Pointer to the device memory location that is used to store the matrix
 * @param cRows		Number of rows of the matrix, since it is always square it is the same as cColumns.
 */
__global__ void initIdentityGPU(float *matrix, int cRows) {
	int x = blockDim.x * blockIdx.x + threadIdx.x;
	int y = blockDim.y * blockIdx.y + threadIdx.y;
	
	if(y < cRows && x < cRows) {
		if (x == y)
			matrix[y * cRows + x] = 1;
		else
			matrix[y * cRows + x] = 0;
	}
}

/**
 * Prints a matrix stored in host memory to stdout.
 *
 * @param A 		Pointer to the host memory location that is used to store the matrix
 * @param nRows		Number of rows of the matrix
 * @param nCols		Number of columns of the matrix
 * @param start		Can be used to specify an iteration when A contains multiple iterations, default is 0
 */
void printMatrix(float *A, int nRows, int nCols, int start) {
	int st = start * nRows * nCols;

	for(int i = 0; i < nRows; i++) {
		for(int j = 0; j < nCols; j++) {
			printf("%f ", A[st + j * nRows + i]);
		}
		printf("\n");
	}
	
	printf("\n");
}

/**
 * Normalizes the columns of a matrix stored on GPU.
 *
 * @param handle	Handle to the CUBLAS library
 * @param A 		Pointer to the device memory location that contains the matrix, it is overwritten with the result
 * @param nRows		Number of rows of the matrix
 * @param nCols		Number of columns of the matrix
 */
void normc(cublasHandle_t &handle, float *A, int nRows, int nCols) {
	for(int i = 0; i < nCols; i++) {
		float norm;
		cublascall(cublasSnrm2(handle, nRows, &A[i * nRows], 1, &norm));
		float mult = 1 / norm;
		cublascall(cublasSscal(handle, nRows, &mult, &A[i * nRows], 1)); 
	}
}
#endif
